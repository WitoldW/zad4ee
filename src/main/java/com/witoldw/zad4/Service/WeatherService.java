
package com.witoldw.zad4.Service;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.witoldw.zad4.Domain.ActualWeather;
import java.io.File;
import java.io.IOException;
import org.codehaus.jackson.map.ObjectMapper;


public class WeatherService {
    
    private final String API_KEY = "cb854cc71b1e1ee22f886f9e30cbb585";
    
    public ActualWeather getActualWeather(String city) throws IOException{
        ClientConfig clientConfig = new DefaultClientConfig();
        Client client = Client.create(clientConfig);
        WebResource webResource = client.resource("http://api.openweathermap.org/data/2.5/weather?q="+city+"&APPID="+API_KEY);
        
        String response = webResource.get(String.class);
        
        ObjectMapper mapper = new ObjectMapper();
        ActualWeather wet = mapper.readValue(response, ActualWeather.class);
        return wet;
    }
}
